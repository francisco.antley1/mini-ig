import { useState, useCallback } from 'react';
import { createApi } from 'unsplash-js';

const api = createApi({
    accessKey: 'CaEWWx4oot3GJm5Z1DSIJq5wusoox3yy5MHvh2G8Lks'
})

const getPhotos = async (options) => {
    const response = await api.search.getPhotos(options)
    return response
}

export const useGetPhotos = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState(null)
    const [data, setData] = useState(null)
    
    const execute = async (options = {}) => {
        try {
            setIsLoading(true);
            const todos = await getPhotos(options)
            setData(todos)
            return todos
        } catch (e) {
            setError(e)
            setIsLoading(false)
            throw e
        }
    };
    
    return {
        isLoading,
        error,
        data,
        execute: useCallback(execute, []),
    }
}