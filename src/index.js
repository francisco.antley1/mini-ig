import ReactDOM from 'react-dom'
import React from 'react'
import Slide from './components/Slide/Slide'
import Feed from './components/Feed/Feed'
import Footer from './components/Footer/Footer'
import './css/index.css'

const Home = () => {
  return (
    <main className="root">
      <div className="wrapper">
        <h1>Discover</h1>
        <Slide/>
        <Feed/>
        <Footer/>
      </div>
    </main>
  )
}

ReactDOM.render(<Home />, document.getElementById('root'))
