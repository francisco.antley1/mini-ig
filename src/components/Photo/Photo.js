import React from 'react'
import 'w3-css/w3.css'
import './css/styles.css'

const Photo = ({ photo, showFullCredit }) => {
    const { user, urls } = photo
    
    return (
        <div className="container">
            <img className="img" src={urls.regular} alt="unsplash photos"/>
            <div className="caption">
                {showFullCredit ? (
                    <>
                        <img src={user.profile_image.medium} alt={user.username} className="w3-left w3-circle" style={{ width: '40px', margin:'5px 10px' }} />
                        <a
                            className="credit"
                            target="_blank"
                            href={`https://unsplash.com/@${user.username}`}
                            rel="noreferrer"
                        >
                            {user.name} <br />
                            <span className="w3-opacity">@{user.username}</span>
                        </a>
                    </>
                ) : (
                        <a
                            className="credit"
                            target="_blank"
                            href={`https://unsplash.com/@${user.username}`}
                            rel="noreferrer"
                        >
                            <span>@{user.username}</span>
                        </a>
                )}
            </div>
        </div>
    )
}

export default Photo
