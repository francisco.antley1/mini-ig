import React from 'react'

import 'w3-css/w3.css';
import './css/styles.css';

import HomeLogo from './assets/home.svg'
import SearchLogo from './assets/search.svg'
import AddLogo from './assets/add.svg'
import CommentLogo from './assets/comment.svg'
import UserLogo from './assets/user.svg'

const Footer = () => {
    return (
        <div className="Footer">
            <div className="w3-bar w3-border w3-center">
                <span href="/">
                    <img src={HomeLogo} alt="Home" className="w3-bar-item w3-button"/>
                </span>
                <span href="/">
                    <img src={SearchLogo} alt="Search" className="w3-bar-item w3-button"/>
                </span>
                <span href="/" className="add-btn">
                    <img src={AddLogo} alt="Add" className="w3-bar-item w3-button"/>
                </span>
                <span href="/">
                    <img src={CommentLogo} alt="Add" className="w3-bar-item w3-button"/>
                </span>
                <span href="/">
                    <img src={UserLogo} alt="Add" className="w3-bar-item w3-button"/>
                </span>
            </div>
        </div>
    );
}

export default Footer;

