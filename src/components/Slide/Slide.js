import React, { useEffect } from 'react'

import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import Photo from '../Photo/Photo'
import { useGetPhotos } from '../../APIUtility'

import './css/styles.css'

const Slide = () => {
    const {
        isLoading,
        data,
        execute,
    } = useGetPhotos();

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    useEffect(() => {
        try {
            execute({
                query: 'cities',
                perPage: 8,
                orderBy: 'relevant'
            })
        } catch (error) {

        }

    }, [execute])

    if (isLoading === null) {
        return <div><p><i class="fa fa-spinner w3-spin" style={{ fontSize: '64px'}}></i></p></div>;
    }

    return (
        <>
            <h2>WHAT'S NEW TODAY</h2>
            <Slider {...settings} >
                {data && data.response.results.map(photo => (
                    <Photo photo={photo} showFullCredit={true} key={photo.id}/>
                ))}
            </Slider>
        </>
    )

}

export default Slide